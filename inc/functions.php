<?php
session_start();
include_once 'conf.php';
require_once 'jdf.php';

/**********      AUTHORIZE Functions  **********/

function isValidAjaxRequest()
{
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        return true;
    }
    return false;
}

function isValidMessage($uName, $uPhone, $uText, &$errorMsg ){
    $errorMsg = '';
    $hasError = false;
    if (strlen($uName) < KA_UNAME_MIN_LENGTH){
        $errorMsg .= 'نام وارد شده کوتاه است.\n';
        $hasError = true;
    }
    if (!is_numeric($uPhone)){
            $errorMsg .= 'شماره تلفن اشتباه است.\n';
            $hasError = true;
    }
    if (strlen($uText) < KA_MESSAGE_MIN_LENGTH){
        $errorMsg .= 'پیام کوتاه است.\n';
        $hasError = true;
    }
    if ($hasError){
        return false;
    }
    return true;
}

function isValidStatus($status)
{
    $statusArr = array('pending', 'published', 'answered');
    if (in_array($status, $statusArr))
        return true;
    return false;
}

function getValidStatus($status)
{
    if (isValidStatus($status)) {
        return $status;
    } else {
        return 'all';
    }
}

/**********  End AUTHORIZE Functions  **********/
/**********   SHOWING Functions  **********/
/**********  End SHOWING Functions  **********/

/**********      Database Functions  **********/


function getBooks()
{
    global $db;
    $sql = "SELECT * FROM $db->books ORDER BY `id` DESC LIMIT 0, 4";
    $result = $db->prepare($sql);
    if ($result) {
        $result->execute();
        $books = $result->fetchAll();
        return $books;
    }
    $errorMsg = 'کتابی موجود نیست';
    return null;
}

function getBook($id){
    global $db;
    $sql = "SELECT * FROM $db->books WHERE `id` = '$id' ";
    $result = $db->prepare($sql);
    if ($result) {
        $result->execute();
        $book = $result->fetch();
        return $book;
    }
    $errorMsg = 'کتابی با این نام موجود نیست';
    return null;
}


function getAllBooks()
{
    global $db;
    $sql = "SELECT * FROM $db->books ORDER BY `id` ";
    $result = $db->prepare($sql);
    if ($result) {
        $result->execute();
        $allBooks = $result->fetchAll();
        return $allBooks;
    }
    $errorMsg = 'کتابی موجود نیست';
    return null;
}

function getAnimations (){
    global $db;
    $sql = "SELECT * FROM $db->ani ORDER BY `id` ASC";
    $result = $db->prepare($sql);
    if ($result){
        $result->execute();
        $animations = $result->fetchAll();
        return $animations;
    }
    $errorMsg = 'کارتونی موجود نیست';
    return null;
}

function getAnimation($id){
    global $db;
    $sql = "SELECT * FROM $db->ani WHERE `id` = '$id' ";
    $result = $db->prepare($sql);
    if ($result) {
        $result->execute();
        $book = $result->fetch();
        return $book;
    }
    $errorMsg = 'کارتونی با این نام موجود نیست';
    return null;
}


function seoFunc($page){
    global $db;
    $sql = "SELECT * FROM $db->seo WHERE `pagename` = '$page' ";
    $result = $db->prepare($sql);
    if ($result) {
        $result->execute();
        $seo = $result->fetch();
        return $seo;
    }
    $errorMsg = 'صفحه ایی با این نام موجود نیست';
    return null;
}

function addMessage($uName, $uAge, $uMail, $uPhone, $uText, &$errorMsg =''){
    global $db;
    list($uName, $uAge, $uMail, $uPhone, $uText) = array(strip_tags($uName), strip_tags($uAge), strip_tags($uMail), strip_tags($uPhone), strip_tags($uText));
    $sql = "INSERT INTO $db->payam (`uname`, `uage`, `umail`, `uphone`, `utext`) values ( :uname, :uage, :umail, :uphone, :utext)";
    $result = $db->prepare($sql);
    $result->bindparam(':uname',$uName);
    $result->bindparam(':uage',$uAge);
    $result->bindparam(':umail',$uMail);
    $result->bindparam(':uphone',$uPhone);
    $result->bindparam(':utext',$uText);
    $result->execute();
    if ($result){
        return true;
    }
    $errorMsg = 'اشکالی در درج پیام رخ داد.';
    return null;
}

function addQuestion($uName, $uAge, $uMail, $uPhone, $uText, &$errorMsg =''){
    global $db;
    list($uName, $uAge, $uMail, $uPhone, $uText) = array(strip_tags($uName), strip_tags($uAge), strip_tags($uMail), strip_tags($uPhone), strip_tags($uText));
    $sql = "INSERT INTO $db->questions (`uname`, `uage`, `umail`, `uphone`, `utext`) values ( :uname, :uage, :umail, :uphone, :utext)";
    $result = $db->prepare($sql);
    $result->bindparam(':uname',$uName);
    $result->bindparam(':uage',$uAge);
    $result->bindparam(':umail',$uMail);
    $result->bindparam(':uphone',$uPhone);
    $result->bindparam(':utext',$uText);
    $result->execute();
    if ($result){
        return true;
    }
    $errorMsg = 'اشکالی در درج سوال رخ داد.';
    return null;
}

function addAnswer($qid, $aText, &$errorMsg = '')
{
    global $db;
    // sanitize all inputs in one line !!!
    list($qid, $aText) = array(strip_tags($qid), strip_tags($aText));
    $sql = "INSERT INTO $db->answers (`qid`, `atext`) VALUES (:qid, :atext);";
    $result = $db->prepare($sql);
    $result->bindparam(':qid',$qid);
    $result->bindparam(':atext',$aText);
    $result->execute();
    if ($result) {
        changeQuestionStatus($qid, 'answered');
        // get email of question and send a noification Email ! [using php mail() function]
        // get mobile number of question and send a noification SMS Here ! [using webservice]
        return true;
    }
    $errorMsg = 'خطایی در هنگام ثبت پاسخ شما رخ داده است .';
    return false;
}


function getAnswers($qid)
{
    global $db;
    $sql = "SELECT * FROM $db->answers where qid='$qid'";
    $result = $db->prepare($sql);
    if ($result) {
        $result->execute();
        $answers = $result->fetchAll();
        $str = '';
        foreach ($answers as $a) {
            $str .= '<div class="a" id="a-' . $a['id'] . '">'
                . nl2br($a['atext']) . '
                <span class="date">(' . jdate(KA_DATE_FORMAT, strtotime($a['created_date'])) . ')
                </span>
                </div>' . PHP_EOL;
        }
        return $str;
    }
    return '';
}

function removeQuestion($qid)
{
    global $db;
    $sql = "Delete from $db->questions WHERE id=$qid;";
    $result = $db->prepare($sql);
    if ($result) {
        $result->execute();
        return true;
    }
    return false;
}

function removeAnswer($aid)
{
    global $db;
    $sql = "Delete from $db->answers WHERE id=$aid;";
    $result = $db->prepare($sql);
    if ($result) {
        $result->execute();
        return true;
    }
    return false;
}

function getLastpost(){
    global $db;
    $sql = "SELECT * FROM $db->books ORDER BY `id` DESC LIMIT 0, 1 ";
    $result = $db->prepare($sql);
    if ($result){
        $result->execute();
        $lastPost = $result->fetch();
        return $lastPost;
    }
    $errorMsg = 'اشکالی رخ داده است.';
    return null;
}


function getQuestions($status = 'all', $search = null, $qpn = 1, &$numQuestions = 0)
{
    global $db;
    // questionPageNumber calculation
    $start = ($qpn - 1) * KA_QUSETION_PER_PAGE;
    $questionPerPage = KA_QUSETION_PER_PAGE;

    if ($status == 'all') {
        if (!isAdmin()) {
            $whereStr = "status!='pending'";
        } else {
            $whereStr = "1";
        }
        if ($search != null) {
            $sql = "SELECT * FROM $db->questions where $whereStr and utext like '%$search%' order by create_date desc limit $start,$questionPerPage";
            $countSql = "SELECT count(*) as c FROM $db->questions where $whereStr and utext like '%$search%'";
        } else {
            $sql = "SELECT * FROM $db->questions where $whereStr order by create_date desc limit $start,$questionPerPage";
            $countSql = "SELECT count(*) as c FROM $db->questions where $whereStr";
        }
    } elseif (isAdmin() or (in_array(getValidStatus($status), array('published', 'answered')))) {
        if ($search != null) {
            $sql = "SELECT * FROM $db->questions where status='$status' and utext like '%$search%' order by create_date desc limit $start,$questionPerPage";
            $countSql = "SELECT count(*) as c FROM $db->questions where status='$status' and utext like '%$search%'";
        } else {
            $sql = "SELECT * FROM $db->questions where status='$status' order by create_date desc limit $start,$questionPerPage";
            $countSql = "SELECT count(*) as c FROM $db->questions where status='$status'";
        }
    } else {
        $sql = "SELECT * FROM $db->questions where status!='pending' order by create_date desc limit $start,$questionPerPage";
        $countSql = "SELECT count(*) as c FROM $db->questions where status!='pending'";
    }
    $result = $db->prepare($sql);
    if ($result) {
        $result->execute();
        $questions = $result->fetchAll();
        $numQuestions = $db->query($countSql)->fetchObject()->c;
        return $questions;
    }
    return null;
}


function geTutorial($search = null, $tpn = 1, &$numTutorial = 0){
    global $db;
    $start = ($tpn - 1) * KA_QUSETION_PER_PAGE;
    $tutorialPerPage = KA_QUSETION_PER_PAGE;

    if ($search != null) {
        $sql = "SELECT * FROM $db->tutorial WHERE `descrip` LIKE '%$search%' OR `title` LIKE '%$search%' ORDER BY created_date DESC limit $start,$tutorialPerPage";
        $countSql = "SELECT count(*) as c FROM $db->tutorial WHERE `descrip` LIKE '%$search%' OR `title` LIKE '%$search%'";
    } else {
        $sql = "SELECT * FROM $db->tutorial ORDER BY created_date DESC limit $start,$tutorialPerPage";
        $countSql = "SELECT count(*) as c FROM $db->tutorial";
    }
    $result = $db->prepare($sql);
    if ($result){
        $result->execute();
        $tutorial = $result->fetchAll();
        $numTutorial = $db->query($countSql)->fetchObject()->c;
        return $tutorial;
    }
    return null;
}

function getAttraction($search = null, $apn = 1, &$numAttraction = 0){
    global $db;
    $start = ($apn - 1) * KA_QUSETION_PER_PAGE;
    $attractionPerPage = KA_QUSETION_PER_PAGE;

    if ($search != null) {
        $sql = "SELECT * FROM $db->attraction WHERE `descrip` LIKE '%$search%' OR `title` LIKE '%$search%' ORDER BY created_date DESC limit $start,$attractionPerPage";
        $countSql = "SELECT count(*) as c FROM $db->attraction WHERE `descrip` LIKE '%$search%' OR `title` LIKE '%$search%'";
    } else {
        $sql = "SELECT * FROM $db->attraction ORDER BY created_date DESC limit $start,$attractionPerPage";
        $countSql = "SELECT count(*) as c FROM $db->attraction";
    }
    $result = $db->prepare($sql);
    if ($result){
        $result->execute();
        $attraction = $result->fetchAll();
        $numAttraction = $db->query($countSql)->fetchObject()->c;
        return $attraction;
    }
    return null;
}



function changeQuestionStatus($qid, $status)
{
    if (isValidStatus($status)) {
        global $db;
        $sql = "UPDATE $db->questions SET status='$status' WHERE id=$qid;";
        $result = $db->prepare($sql);
        if ($result) {
            $result->execute();
            return true;
        }
    }
    return false;
}

/**********  End Database Functions  **********/
/**********      SHOWING Functions  **********/

/**********  End SHOWING Functions  **********/
/***** Authentication(login/logout/check) Functions *****/
function doLogin($username, $password)
{
    if ($username == KA_ADMIN_USERNAME && $password == KA_ADMIN_PASSWORD) {
        $_SESSION['login'] = true;
        $_SESSION['user'] = $username;
        $_SESSION['userIP'] = $_SERVER['REMOTE_ADDR'];
        return true;
    }
    return false;
}

function doLogout()
{
    unset($_SESSION['login'], $_SESSION['user'], $_SERVER['REMOTE_ADDR']);
    return true;
}

function isAdmin()
{
    return (isset($_SESSION['login'])) ? true : false;
}


/***** Pagination Functions *****/
function getNumPages($numQuestions)
{
    $numPages = ceil($numQuestions / KA_QUSETION_PER_PAGE);
    return $numPages;
}

function getPageUrl($pageNumber)
{
    $getParameters = array();
    if (isset($_GET['status']))
        $getParameters['status'] = $_GET['status'];
    if (isset($_GET['search']))
        $getParameters['search'] = $_GET['search'];
    $getParameters['qpn'] = $pageNumber;
    $str = '&';
    foreach ($getParameters as $key => $value) {
        $str .= "$key=$value&";
    }
    return KA_QUESTION_URL . substr($str, 0, -1);
}

function getNumPagesTut($numTutorial)
{
    $numPagesTut = ceil($numTutorial / KA_QUSETION_PER_PAGE);
    return $numPagesTut;
}
function getPageUrlTut($pageNumber)
{
    $getParameters = array();
    if (isset($_GET['searchtut']))
        $getParameters['searchtut'] = $_GET['searchtut'];
    $getParameters['tpn'] = $pageNumber;
    $str = '&';
    foreach ($getParameters as $key => $value) {
        $str .= "$key=$value&";
    }
    return KA_TUTORIAL_URL . substr($str, 0, -1);
}



function getNumPagesAtt($numAttraction)
{
    $numPagesAtt = ceil($numAttraction / KA_QUSETION_PER_PAGE);
    return $numPagesAtt;
}
function getPageUrlAtt($pageNumber)
{
    $getParameters = array();
    if (isset($_GET['searchatt']))
        $getParameters['searchatt'] = $_GET['searchatt'];
    $getParameters['apn'] = $pageNumber;
    $str = '&';
    foreach ($getParameters as $key => $value) {
        $str .= "$key=$value&";
    }
    return KA_ATTRACTION_URL . substr($str, 0, -1);
}