<?php
include_once 'functions.php';
$errorMsg = false; // error message
$successMsg = false; // success message


/********     BOOK  ********/
$books = getBooks();  /* it returns only last four books for showing at home page */

$allBooks = getAllBooks();  /* it returns all books at books page */

if (isset($_GET['page'])=="book") {   /* it returns only one book ,ready to read */
    if (isset($_GET['id'])){
        $book = getBook($_GET['id']);
    }
}
/********  END BOOK  ********/



/********      ANIMATION  ********/
$animations = getAnimations();

if (isset($_GET['page'])=="animation") {
    if (isset($_GET['id'])){
        $animation = getAnimation($_GET['id']);
    }
}
/********  END ANIMATION  ********/



/********  SEO  ********/
if (!isset($_GET['page'])) {
    $page = 'home';
    $seo = seoFunc($page);
}else{
    $page = $_GET['page'];
    $seo = seoFunc($page);
}
/********  END SEO  ********/

/********      TUTORIAL         ********/
// get Tutorial
$tutorial = null;
$numTutorial = 0;
$tpn = (isset($_GET['tpn'])) ? $_GET['tpn'] : 1;
if (isset($_GET['searchtut']) and strlen($_GET['searchtut'])>0) {

    $search = str_ireplace(' ', '%', $_GET['searchtut']);
    $tutorial = geTutorial($search, $tpn, $numTutorial);
} else {
    $tutorial = geTutorial(null, $tpn, $numTutorial);
}
/********  END TUTORIAL  ********/

/********      ATTRACTION  ********/

$attraction = null;
$numAttraction = 0;
$apn = (isset($_GET['apn'])) ? $_GET['apn'] : 1;
if (isset($_GET['searchatt']) and strlen($_GET['searchatt'])>0) {

    $search = str_ireplace(' ', '%', $_GET['searchatt']);
    $attraction = getAttraction($search, $apn, $numAttraction);

} else {

    $attraction = getAttraction(null, $apn, $numAttraction);
}

/********  END ATTRACTION  ********/
/********  QUESTON  ********/
if (isset($_POST['submitQuestion'])){

    if (isValidMessage($_POST['uName'], $_POST['uPhone'], $_POST['uText'], $errorMsg)){
        if (addQuestion($_POST['uName'], $_POST['uAge'], $_POST['uMail'], $_POST['uPhone'], $_POST['uText'])){
            $successMsg = "پیام پر مهر شما به دست ما رسید. از اینکه برای ما وقت گذاشتید سپاسگزاریم.";
            echo "<script>
            setTimeout(function () {
                window.location.href= 'index.php?page=questions'; 

            },4000);
            </script>";
        }

    }
}

if (isset($_POST['submitAnswer']) and isAdmin()) {
    if (addAnswer($_POST['qid'], $_POST['atext'], $errorMsg)) {
        $successMsg = "پاسخ با موفقیت ثبت شد .";
        echo "<script>
            setTimeout(function () {
                window.location.href= 'index.php?page=questions'; 

            },4000);
            </script>";
    }
}

// get questions
$questions = null;
$numQuestions = 0;
$qpn = (isset($_GET['qpn'])) ? $_GET['qpn'] : 1;
if (isset($_GET['search']) and strlen($_GET['search'])>0) {
    /* a simple trick : replace space with % for use in sql Like statement
     * other aproach : like , fulltext , concat
     * Read this pages :  http://dev.mysql.com/doc/refman/5.0/en/fulltext-natural-language.html
                          http://www.mysqltutorial.org/mysql-full-text-search.aspx
     */
    $search = str_ireplace(' ', '%', $_GET['search']);
    if (isset($_GET['status'])) {
        $questions = getQuestions($_GET['status'], $search,$qpn,$numQuestions);
    } else {
        $questions = getQuestions('all', $search,$qpn, $numQuestions);
    }
} else {
    if (isset($_GET['status'])) {
        $questions = getQuestions(trim($_GET['status']), null, $qpn,$numQuestions);
    } else {
        $questions = getQuestions('all', null, $qpn,$numQuestions);
    }
}


/********  END QUESTON  ********/
/********    LOGIN  ********/
if (isset($_POST['submitLogin'])) {
    if (doLogin($_POST['username'], $_POST['password'])) {
        header("Location: " . KA_QUESTION_URL);
    } else {
        $errorMsg = 'نام کاربری یا رمز وارد شده اشتباه است .';
    }
}
if (isset($_GET['logout'])) {
    doLogout();
}
/********  END LOGIN  ********/

/********   CONTACT  ********/
if (isset($_POST['submitMessage'])){
    $d = $_POST['uName'];
    if (isValidMessage($_POST['uName'], $_POST['uPhone'], $_POST['uText'], $errorMsg)){
        if (addMessage($_POST['uName'], $_POST['uAge'], $_POST['uMail'], $_POST['uPhone'], $_POST['uText'])){
            $successMsg = "پیام پر مهر شما به دست ما رسید. از اینکه برای ما وقت گذاشتید سپاسگزاریم.";
            echo "<script>
            setTimeout(function () {
                window.location.href= 'index.php?page=contact'; 

            },4000);
            </script>";
        }

    }
}
/********  END CONTACT  ********/

$lastPost = getLastpost();








function per_number($num)
{
    $eng = array('0','1','2','3','4','5','6','7','8','9','?','-');
    $per = array('۰','۱','۲','۳','۴','۵','۶','۷','۸','۹','؟',' ');
    return str_replace($eng,$per,$num);
}