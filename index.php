<?php
include_once 'inc/actions.php';
include_once 'blocks/head.php';
include_once 'blocks/menu.php';

if (!ISSET($_GET["page"]))
{
    include_once 'blocks/header_div.php';
    include_once 'blocks/kodak_aftab.php';
    include_once 'blocks/book_part.php';
    include_once 'blocks/animation_part.php';
}
else {
    $page=$_GET["page"];
    include("pages/$page.php");
}

include_once 'blocks/last_posts.php';
include_once 'blocks/footer.php';
include_once 'blocks/scripts.php';








