<footer class="container-fluid main align-right bg-basecolor bg-basecolor-footer">
    <div class="row">
        <div class="col-xl-4 col-md-4 col-sm-12 col-xs-12 footer">
            <div class="goal">
                <h3 class="wow jello">ما را دنبال کنید</h3>
                <p>
                    آیا میدانستید که در کانال تلگرامی کودک آفتاب روانشناسان با تجربه کودک بصورت رایگان و داوطلبانه به شما مشاوره میدهند !
                </p>
                <p>
                    شما میتوانید با عضویت در شبکه های اجتماعی ما به صورت کاملا رایگان کمک های مشاوران با تجربه ما را در زندگیتان داشته باشید.
                </p>
                    <div class="share-footer">
                        <ul>
                            <li><a href="<?php echo KA_ADDRESS_LINKEDIN; ?>"><i class="flaticon-linkedin"></i></a></li>
                            <li><a href="<?PHP echo KA_ADDRESS_TELEGRAM; ?>"><i class="flaticon-telegram"></i></a></li>
                            <li><a href="<?PHP echo KA_ADDRESS_WHATSAPP; ?>"><i class="flaticon-whatsapp"></i></a></li>
                            <li><a href="<?PHP echo KA_ADDRESS_INSTAGRAM; ?>"><i class="flaticon-instagram"></i></a></li>
                        </ul>
                    </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="col-xl-4 col-md-4 col-sm-12 col-xs-12 footer">
            <div class="goal">
                <h3 class="wow jello">مثبت اندیشی برای کودکان</h3>
                <p>
                    مثبت گرایی یعنی رضایت بخش تر کردن زندگی روزمره ، یعنی داشتن احساس بهتری نسبت به خودم و زندگیم و هرچه دارم . مثبت گرایی از شما می پرسد چه توانایی هایی دارید و به شما انگیزه می دهد تا این توانایی ها را تقویت کنید . یعنی به شما کمک میکند تا استعدادتان شکوفا شود و در آینده ی نزدیک انسانی موفق و شاد شوید.
                </p>
                <div class="read-more">
                    <a class="btn btn-kodak-yellow" href="<?php echo KA_HOME_URL ?>?page=attraction"> ادامه متن  </a>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="col-xl-4 col-md-4 col-sm-12 col-xs-12 footer">
            <div class="goal">
                <h3 class="wow jello">همکاری با ما</h3>
                  <p>
                      دوستان و همراهان گرامی ، <a href="<?php echo KA_HOME_URL ?>">کودک آفتاب</a> پذیرای هر گونه پیشنهاد همکاری و یا همفکری از طرف شما عزیزان می باشد . پس لطفا در صورت تمایل محبتتان را از ما دریغ نکنید. برای این منظور با این شماره تلفن تماس بگیرید و یا در صفحه
                      <a href="<?php echo KA_HOME_URL; ?>?page=contact">تماس با ما</a> برای ما پیام بگذارید ، در اولین فرصت به پیام شما پاسخ داده خواهد شد.
                  </p>
                <address>
                    <a href="tel:+982144488763"><i class="flaticon-phone"><?php echo per_number(KA_PHONE); ?></i></a>
                </address>

                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="weblic">
            <div class="img-weblic"></div>
            <div><a href="<?PHP echo KA_ADDRESS_LINKEDIN ?>"><h2>طراحی حرفه ایی وبسایت و برنامه های اندروید با گروه وب لیک</h2></a></div>
        </div>
        <div class="clear"></div>
    </div>
</footer>
</div>
