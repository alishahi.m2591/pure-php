<div class="container-fluid last-news">
    <div class="container align-right position-relative">
        <h3 class="text-shadow-lower animated infinite tada">تازه ها در <a href="<?php echo KA_HOME_URL; ?>"><strong class="hover-theme ">کودک آفتاب</strong></a></h3>

        <div class="Top z-depth-1-half"></div>

        <div class="Tabselected col-xl-3 col-md-3 col-sm-6"><img class="z-depth-1-half" src="img/<?php echo $lastPost["image"];?>" alt="داستان بچگانه آموزنده"></div>
        <div class="Tabnews col-xl-9 col-md-9 col-sm-6">
            <div class="last-news-title"><h3><?php echo $lastPost["title"];?></h3></div>
            <div class="last-news-desc"><p> <?php echo $lastPost["description"];?> </p> </div>
        </div>
        <div class="last-news-btn oval z-depth-1-half">
            <a href="<?php echo KA_HOME_URL; ?>?page=books"> بیشتر<img src="img/elipsis.png" alt="قصه کودکانه"></a>
        </div>
    </div>
    <div class="clear"></div>
</div>