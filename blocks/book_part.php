<div class="contner-fluid picbook">
    <div class="container">
        <div class="align-right text-shadow-lower">
            <h2>
                کتاب بخون و یاد بگیر!
            </h2>
        </div>
        <div class="card-deck">
            <?php
            $num=0;
            foreach ($books as $b):
            $num+=2;
            ?>
                <div class="col-xl-3 col-sm-6 col-md-3 col-xs-12">
                    <div  class="card z-depth-1-half wow fadeInUp" data-wow-delay="<?php echo $num; ?>00ms">
                        <!--Card image-->
                        <div class="view overlay">
                            <img class="card-img-top z-depth-1-half" src="img/<?php echo $b['image'] ?>" alt="کتاب قصه صوتی رایگان <?php echo $b['title'] ?>">
                        </div>
                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <a class="border-none" href="<?php echo KA_HOME_URL ?>?page=book&id=<?php echo $b["id"]; ?>"><h4 class="card-title text-shadow-lower"><span>کتاب قصه </span><strong><?php echo $b['title'] ?></strong></h4></a>
                            <!--Text-->
                            <p class="card-text"><?php echo per_number($b['description']) ?></p>
                            <!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
                            <a class="btn z-depth-1-half" href="<?php echo KA_HOME_URL ?>?page=book&id=<?php echo $b["id"]; ?>">
                                خواندن کتاب
                            </a>
                        </div>
                    </div>
                </div>

            <?php endforeach; ?>
            <a class="more-books z-depth-1-half wow zoomIn" data-wow-delay="1s" href="<?php echo KA_HOME_URL; ?>?page=books">
                کتابخانه کودک آفتاب
            </a>
        </div>

    </div>
    <div class="clear"></div>
</div>