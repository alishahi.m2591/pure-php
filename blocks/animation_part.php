<div class="container-fluid body bg-basecolor">
    <div class="container ">
        <div class="main-title text-shadow">
            <h2 class="wow lightSpeedIn" data-wow-delay="500ms">
                تماشای انیمیشن های ما
            </h2>
        </div>
        <div class="slider">
            <?php foreach ($animations as $a): ?>
                <figure class="slider__item wow zoomIn">
                    <a href="<?php echo KA_HOME_URL ?>?page=animation&id=<?php echo $a["id"]?>">
                        <img class="slider__image" alt="<?php echo $a["title"]?>"  src="img/<?php echo $a['image'] ?>"/>
                    </a>
                    <figcaption class="slider__caption"><?php echo $a['title'] ?></figcaption>
                </figure>
            <?php endforeach; ?>
            <div class="slider__btn"><img src="img/chevron-arrow-down.png" alt="کودک آفتاب ناشر کتاب های صوتی و تصویری کودکان"></div>
        </div>
    </div>
    <div class="clear"></div>
</div>