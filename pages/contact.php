<section class="page bg-contact">
    <div class="container-fluid oth-pages paddmarg">
        <h1 class="text-center text-light"><strong>با گروه کودک آفتاب در تماس بمانید</strong></h1>
        <hr>
        <div class="row" id="contatti">
            <div class="container mt-5" >
                <?php
                if ($errorMsg) {
                echo "<div class='errSuc error'>" . nl2br($errorMsg) . "</div>";
                } elseif ($successMsg) {
                echo "<div class='errSuc success'>" . nl2br($successMsg) . "</div>";
                }
                ?>
                <div class="row contact">
                    <div class="col-md-6 con-text text-light" >
                       <h2 class="wow pulse">
                           <strong>به کودک آفتاب بپیوندید</strong>
                       </h2>
                       <p>
                           اگر پیام ، پیشنهاد و یا انتقادی دارید که ما را در ادامه راه یاری می دهد لطفا ما را آگاه کنید. از هر راهی که برای شما آسانتر است با ما در تماس باشید.
                       </p>
                        <div class="share2">
                            <p><strong>ما را در شبکه های اجتماعی دنبال کنید...</strong></p>
                            <ul>
                                <li class="wow zoomIn" data-wow-delay="200ms"><a href="<?php echo KA_ADDRESS_LINKEDIN ?>"><img src="../img/linkedin-2.png" alt="کودک آفتاب ناشر کتاب آنلاین کودکان"></a></li>
                                <li class="wow zoomIn" data-wow-delay="400ms"><a href="<?PHP echo KA_ADDRESS_TELEGRAM ?>"><img src="../img/telegram-g.png" alt=" کانال تلگرام کودک آفتاب قوانین جذب برای کودکان"></a></li>
                                <li class="wow zoomIn" data-wow-delay="600ms"><a href="<?PHP echo KA_ADDRESS_WHATSAPP ?>"><img src="../img/whatsap.png" alt="کودک آفتاب خودشناسی و روانشناسی کودک"></a></li>
                                <li class="wow zoomIn" data-wow-delay="800ms"><a href="<?PHP echo KA_ADDRESS_INSTAGRAM ?>"><img src="../img/instagram.png" alt="قانون جذب و مثبت اندیشی برای کودکان اینستاگرام کودک آفتاب"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 con-text">
                        <h2 class="text-light wow pulse">
                            <strong>پیام بفرستید</strong>
                        </h2>
                        <form action="#" method="post">
                            <script>document.querySelector("form").setAttribute("action", "")</script>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="uName" class="form-control mt-2" placeholder="نام شما چیست؟" tabindex="2" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="text" name="uAge" class="form-control mt-2" placeholder="چند سال دارید؟" tabindex="3" required>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="email" name="uMail" class="form-control mt-2" placeholder="ایمیل خود را بنویسید" tabindex="4">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <input type="tel" name="uPhone" class="form-control mt-2" placeholder="شماره تلفن خود را وارد کنید" tabindex="5" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea name="uText" class="form-control" id="exampleFormControlTextarea1" tabindex="6" placeholder="پیام شما هر چه باشد برای ما پر از مهر است . لطفا پیام خود را اینجا بنویسید ." rows="3" required></textarea>
                                    </div>
                                </div>
                                <div class="col-12 text-left wow zoomIn" data-wow-delay="1000ms">
                                    <input name="submitMessage" class="btn btn-outline-light" type="submit" tabindex="7" value="فرستادن">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row text-center bg-success text-white author">
            <div id="adv" class="col-12 mt-4 h3 wow zoomIn" data-wow-delay="1200ms">
                <a id="adv-a" href="#">با مجموعه ما بیشتر آشنا شوید</a>
                <a href="<?php echo KA_ADDRESS_LINKEDIN ?>"><img id="adv-img" src="../img/wealth-green.png" alt="مریم علیشاهی برنامه نویس ارشد"></a>
            </div>
        </div>
        <div class="row" class="mapp">
            <?php echo KA_MAP; ?>
        </div>
        <div class="row text-center">
            <div class="col-4 box1 pt-4">
                <a href="tel:+982144488763"><i class="fas flaticon-old-handphone fa-3x"></i>
                    <h3 class="d-none d-lg-block d-xl-block text-center">تلفن:</h3>
                    <p class="d-none d-lg-block d-xl-block text-center ltr-dir"><?php echo per_number(KA_PHONE); ?></p></a>
            </div>
            <div class="col-4 box2 pt-4">
                <a href="<?php echo KA_HOME_URL; ?>"><i class="fas flaticon-home-interface fa-3x"></i>
                    <h3 class="d-none d-lg-block d-xl-block text-center">کدپستی:</h3>
                    <p class="d-none d-lg-block d-xl-block text-center ltr-dir"><?php echo per_number(KA_POSTAL_KODE); ?></p></a>
            </div>
            <div class="col-4 box3 pt-4">
                <a href="mailto:message@kodakeaftab.ir"><i class="fas flaticon-envelope fa-3x"></i>
                    <h3 class="d-none d-lg-block d-xl-block text-center">ایمیل:</h3>
                    <p class="d-none d-lg-block d-xl-block text-center">message@kodakeaftab.ir</p>
                </a>
            </div>
        </div>
    </div>



    <div class="row text-center bg-success text-light author">
        <div class="col-12 mt-4 h3 ">
            <div class="share2 share3 wow zoomIn">
                <ul>
                    <li><a href="<?php echo KA_ADDRESS_LINKEDIN ?>"><img src="../img/linkedin-w.png" alt="کودک آفتاب سایت کودک و نوجوان در لینکدین"></a></li>
                    <li><a href="<?PHP echo KA_ADDRESS_TELEGRAM ?>"><img src="../img/telegram-w.png" alt="کانال کودک آفتاب ایجاد انگیزه در کودکان و نوجوانان در تلگرام"></a></li>
                    <li><a href="<?PHP echo KA_ADDRESS_WHATSAPP ?>"><img src="../img/whatsapp-w.png" alt="تماس با کودک آفتاب و مریم علیشاهی"></a></li>
                    <li><a href="<?PHP echo KA_ADDRESS_INSTAGRAM?>"><img src="../img/instagram-w.png" alt="کودک آفتاب را در اینستاگرام دنبال کنید"></a></li>
                </ul>
            </div>
        </div>
    </div>
</section>

